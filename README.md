# Description

Developed Android application where organisers of the particular event register the participants through their unique QR code and data is sent to main server when connected to college network for National level techno-cultural fest of NMAM Institute of Technology, Nitte. 

## The Front Login for Organisers of the Event.

![Screenshot__218_](/uploads/257f20a56f668e316218668438d1d0e6/Screenshot__218_.png)

## The Manger Screen 

![Screenshot__217_](/uploads/89c4787522f8865706dbc52a81931897/Screenshot__217_.png)

## QR Code Scanner

![Screenshot__219_](/uploads/d3dc60a692c832ecea45bd307958e31c/Screenshot__219_.png)