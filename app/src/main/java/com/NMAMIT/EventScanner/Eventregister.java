package com.NMAMIT.EventScanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.blikoon.qrcodescanner.QrCodeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.spec.ECField;
import java.util.LinkedList;
import java.util.List;

public class Eventregister extends AppCompatActivity {
    private static final int REQUEST_CODE_QR_SCAN = 101;
    private static final String LOGTAG = "error log";
    private Context mContext;
    //SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

    TextView t1,t2,regcount,sentcount;
    EditText pidtext;
    Button bqr,badd,breg;
    ImageButton blogout,bback;

    LinkedList<String> Pids;
    String Cid;

    boolean qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventregister);
        t1=findViewById(R.id.participantview);
        t1.setMovementMethod(new ScrollingMovementMethod());
        t2=findViewById(R.id.textView);
        bqr=findViewById(R.id.qrcode);
        badd=findViewById(R.id.badd);
        pidtext=findViewById(R.id.pidtext);
        blogout=findViewById(R.id.imageButton);
        bback=findViewById(R.id.imageButton2);
        breg=findViewById(R.id.button3);
        regcount=findViewById(R.id.regcount);
        sentcount=findViewById(R.id.sentcount);

        mContext = getApplicationContext();
        SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);

        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Incrideaevent(Eventname VARCHAR(30),Participants VARCGAR(100) PRIMARY KEY,sent int(1))");




        SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

        String s1 = sh.getString("eventname", "");

        //System.out.println(s1);
        //String s1 = sh.getString("eventname", "");

        t2.setText(s1);

        resetlist();
        doback();


       /* Thread thread = new Thread()
        {
            @Override
            public void run() {

                while (true)
                {
                    SharedPreferences sharedPreferences = getSharedPreferences("Incridea", MODE_PRIVATE);

                    final SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);

                    Cursor resultSet = mydatabase.rawQuery("Select * from Incrideaevent where sent = 0 Limit 1",null);

                    if (resultSet.getCount()>0)
                    {
                        resultSet.moveToFirst();

                        String evename = resultSet.getString(0);
                        final String partici = resultSet.getString(1);
                        int ss=resultSet.getInt(2);

                        String iip=sharedPreferences.getString("ipaddress","");

                        System.out.println("result : "+evename+" "+partici+" "+iip);

                        if(ss==0)
                        {
                            String url="http://"+iip+":2020/register?eventname="+evename+"&participants="+partici;

                            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

                            StringRequest stringRequest= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    System.out.println("res :"+response);
                                    SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);
                                    mydatabase.execSQL("update Incrideaevent set sent = 1 where Participants='"+partici+"'");

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    System.out.println("rop :"+error);
                                }
                            });
                            requestQueue.add(stringRequest);

                        }

                    }
                    else
                    {
                        System.out.println("pass");
                        try
                        {
                            sleep(1000);
                        }
                        catch (Exception e)
                        {
                            System.out.println("123");
                        }
                        run();
                    }
                }



            }
        };

        thread.start();*/

        breg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getccount();
                inserttodb();

            }
        });
        bback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Pids.size()>0)
                {
                    Pids.removeLast();
                    displayPids();
                }

            }
        });

        blogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("Incridea", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();

                myEdit.putString("eventname", "");
                myEdit.putInt("min",0);
                myEdit.putInt("max",0);
                myEdit.commit();

                finish();
                Intent i=new Intent(Eventregister.this,MainActivity.class);
                startActivity(i);
            }
        });
        badd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tval=pidtext.getText().toString();
                qr=false;
                addpid(tval);
                pidtext.setText("");
            }
        });
        bqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Eventregister.this, QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);

                qr=true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Log.d(LOGTAG, "COULD NOT GET A GOOD RESULT.");
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(Eventregister.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                        }
                        });
                alertDialog.show();

            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            /*Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(Eventregister.this).create();
            alertDialog.setTitle("Scan result");
            alertDialog.setMessage(result);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();*/
            //t1.append(result + "\n");
            qrdetected(result);
        }

}

    private void doback()
    {
        //while (true)
        {
            getccount();
            SharedPreferences sharedPreferences = getSharedPreferences("Incridea", MODE_PRIVATE);

            SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);

            Cursor resultSet = mydatabase.rawQuery("Select * from Incrideaevent where sent = 0 Limit 1",null);

            if (resultSet.getCount()>0)
            {
                resultSet.moveToFirst();

                String evename = resultSet.getString(0);
                final String partici = resultSet.getString(1);
                int ss=resultSet.getInt(2);

                String iip=sharedPreferences.getString("ipaddress","");

                System.out.println("result : "+evename+" "+partici+" "+iip);

                if(ss==0)
                {
                    String url="http://"+iip+":2020/register?eventname="+evename+"&participants="+partici;

                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);

                    StringRequest stringRequest= new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            System.out.println("res :"+response);
                            SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);
                            mydatabase.execSQL("update Incrideaevent set sent = 1 where Participants='"+partici+"'");
                            doback();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("rop :"+error);

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    doback();
                                }
                            }, 1000);
                        }
                    });
                    requestQueue.add(stringRequest);

                }

            }
            else
            {
                System.out.println("pass");

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doback();
                    }
                }, 1000);
            }
        }

    }

    private void resetlist()
    {

        Pids=new LinkedList<String>();
        Cid=null;
        t1.setText("");
    }

    private void getccount(){
        SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);
        Cursor resultSet1 = mydatabase.rawQuery("Select * from Incrideaevent",null);
        String o= ""+resultSet1.getCount();
        regcount.setText(o);

        Cursor resultSet2 = mydatabase.rawQuery("Select * from Incrideaevent where sent = 1",null);
        String oo=""+resultSet2.getCount();
        sentcount.setText(oo);

    }

    private void addpid(String val)
    {

        System.out.println("wanrt "+val);
        SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

        if(Pids.size()>=sh.getInt("max",0))
        {
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(Eventregister.this).create();
            alertDialog.setTitle("Count Error");
            alertDialog.setMessage("Team Limit exceeded");
            alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else
        {
            if(!Pids.contains(val))
            {
                val=val.toUpperCase();
                String re="[I][N][-][0-9][0-9][-][0-9][0-9][0-9][0-9]";
                System.out.println("re :"+val);
                if(val.matches(re))
                {
                    /*
                    String cid=val.split("-")[1];
                    if( Cid==null || cid.equals(Cid))
                    {
                        Cid=cid;
                        Pids.add(val);
                        displayPids();
                    }
                    */
                    Pids.add(val);
                    displayPids();
                }
                else
                {
                    System.out.println("res :"+val.matches(re));
                    AlertDialog alertDialog = new AlertDialog.Builder(Eventregister.this).create();
                    alertDialog.setTitle("Invalid Pid Input");
                    alertDialog.setMessage("Please check the PID");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                //101


            }

            if(qr)
            {
                Intent i = new Intent(Eventregister.this, QrCodeActivity.class);
                startActivityForResult( i,REQUEST_CODE_QR_SCAN);
            }

        }
    }

    private void displayPids(){
        String text="";
        for (String pid : Pids) {
           text+=pid+"\n";
        }
        t1.setText(text.toUpperCase());
    }

    public void qrdetected(String decoded)
    {
        addpid(decoded.split("\\|")[1]);

    }

    public void inserttodb()
    {
        SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

     if(Pids.size()<sh.getInt("min",0))
     {
         AlertDialog alertDialog = new AlertDialog.Builder(Eventregister.this).create();
         alertDialog.setTitle("Minimum Error");
         alertDialog.setMessage("Minimum participants have not been reached");
         alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                 new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int which) {
                         dialog.dismiss();
                     }
                 });
         alertDialog.show();
     }
     else
     {
         SharedPreferences sh1 = getSharedPreferences("Incridea", MODE_PRIVATE);

         String s1 = sh1.getString("eventname", "");

         String g="";
         for (String pid : Pids) {
             g+=pid.split("-")[2]+"|";
         }
         SQLiteDatabase mydatabase = openOrCreateDatabase("Incridea",MODE_PRIVATE,null);

         try
         {
             mydatabase.execSQL("INSERT INTO Incrideaevent VALUES('"+s1+"','"+g+"',0)");

         }
         catch (Exception e)
         {
             AlertDialog alertDialog = new AlertDialog.Builder(Eventregister.this).create();
                 alertDialog.setTitle("Participant Error");
                 alertDialog.setMessage("Participant already Registered");
                 alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int which) {
                                 dialog.dismiss();
                             }
                         });
                 alertDialog.show();
         }
        resetlist();
         //doback();
         /*Cursor resultSet = mydatabase.rawQuery("Select * from Incrideaevent",null);

         while (resultSet.moveToNext())
         {

             String username = resultSet.getString(0);
             String password = resultSet.getString(1);
             int ss=resultSet.getInt(2);

             System.out.println("result : "+username+" "+password+" "+ss);

         }*/
     }
    }

}
