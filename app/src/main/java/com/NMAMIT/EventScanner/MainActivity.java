package com.NMAMIT.EventScanner;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class MainActivity extends AppCompatActivity {
    private Context mContext;
    Button Bchangeip,Blogin;
    EditText Eeventtitle,Epassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bchangeip=findViewById(R.id.changeip);
        Blogin=findViewById(R.id.Login);
        Eeventtitle=findViewById(R.id.eventname);
        Epassword=findViewById(R.id.Password);


        mContext = getApplicationContext();
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
        SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

        String s1 = sh.getString("ipaddress", "");
        String s2 = sh.getString("eventname","");
        if(s1.isEmpty())
        {
            gotoippage();
        }
        else if(!s2.isEmpty())
        {
            finish();
            gotoeventregisterpage();
        }



        Bchangeip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoippage();
            }
        });
        Blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eventname=Eeventtitle.getText().toString();
                String epass=Epassword.getText().toString();

                if(eventname.isEmpty() || epass.isEmpty())
                {
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    alertDialog.setTitle("Invalid Input");
                    alertDialog.setMessage("One of the field is empty");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
                else {
                    SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

                    String s1 = sh.getString("ipaddress", "");
                    String s2 = Eeventtitle.getText().toString();
                    String s3 = Epassword.getText().toString();

                    System.out.println("rep123:"+s1);
                    System.out.println("rep123:"+s2);
                    System.out.println("rep123:"+s3);

                    String url="http://"+s1+":2020/login?eventname="+s2+"&password="+s3;

                    //String url="http://www.google.com";

                    RequestQueue requestQueue = Volley.newRequestQueue(mContext);
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                            (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        String event=response.getString("eventname");
                                        int min=response.getInt("min");
                                        int max=response.getInt("max");
                                        //System.out.println("res123"+event+" "+min+" "+max);
                                        SharedPreferences sharedPreferences = getSharedPreferences("Incridea", MODE_PRIVATE);
                                        SharedPreferences.Editor myEdit = sharedPreferences.edit();
                                        myEdit.putString("eventname", event);
                                        myEdit.putInt("min",min);
                                        myEdit.putInt("max",max);
                                        myEdit.commit();

                                        gotoeventregisterpage();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO: Handle error
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                    alertDialog.setTitle("Failed!");
                                    alertDialog.setMessage("Invalid Credentials");
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.show();

                                }
                            });
                    requestQueue.add(jsonObjectRequest);


                }
            }
        });

    }


    public void gotoeventregisterpage()
    {
        Intent i = new Intent(this,Eventregister.class);
        startActivity(i);
    }

    public void gotoippage()
    {
        Intent i = new Intent(this,ippages.class);
        startActivity(i);
    }
}
