package com.NMAMIT.EventScanner;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ippages extends AppCompatActivity {

    TextView ipadd;
    Button bsub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ippages);

        ipadd=findViewById(R.id.ipaddress);
        bsub=findViewById(R.id.submit);

        bsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences("Incridea", MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sharedPreferences.edit();

                myEdit.putString("ipaddress", ipadd.getText().toString());

                myEdit.commit();
                finish();
            }
        });

        SharedPreferences sh = getSharedPreferences("Incridea", MODE_PRIVATE);

        String s1 = sh.getString("ipaddress", "");

        ipadd.setText(s1);




    }
}
